//
//  ServingRoom.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 02.06.24.
//

public struct ServingRoom {
    public enum Change {
        case adding(Adding); public enum Adding {
            case table(Table)
        }
        case changing(Changing); public enum Changing {
            case name(String)
        }
    }
    
    public let name  : String
    public let tables: [Table]
    
    public init(name:String) {
        self.init(name, [])
    }
    
    private init(_ name: String,_ tables: [Table]) {
        self.name = name
        self.tables = tables
    }
    
    public  func alter(by c:Change...) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    private func alter(by c:Change   ) -> Self {
        switch c {
        case let .changing(.name (n)): Self(n   , tables      )
        case let .adding  (.table(t)): Self(name, tables + [t])
        }
    }
}
