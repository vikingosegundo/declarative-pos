//
//  DiskStore.swift
//  ELPModels
//
//  Created by Manuel Meyer on 11.08.22.
//

import Foundation.NSFileManager

public func createDiskStore(
    pathInDocs   p: String      = "state.json",
    fileManager fm: FileManager = .default
) -> Store<AppState, AppState.Change>
{
    var s = loadAppStateFromStore(pathInDocuments:p,fileManager:fm) { didSet { c.forEach { $0() } } } // state
    var c: [() -> ()] = []  // callbacks
    return (
          state:{ s                                                },
         change:{ s = s.alter(by:$0);persist(state:s,at:p,with:fm) },
          reset:{ s = AppState()    ;persist(state:s,at:p,with:fm) },
        updated:{ c = c + [$0] /*add callback*/                    },
        destroy:{ destroyStore(at:p,with:fm)                       }
    )
}

//MARK: -
private func persist(state:AppState,at pathInDocuments:String,with fileManager:FileManager) {
    do {
        let encoder = JSONEncoder()
        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif
        try encoder
            .encode(state)
            .write(to:fileURL(pathInDocuments:pathInDocuments,fileManager:fileManager))
    } catch { print(error) }
}

private func loadAppStateFromStore(pathInDocuments:String, fileManager:FileManager) -> AppState {
    do {
        let fu = fileURL(pathInDocuments:pathInDocuments,fileManager:fileManager)
        return try JSONDecoder().decode(AppState.self,from:try Data(contentsOf:fu))
    } catch {
        print(error)
        return AppState()
    }
}

private func destroyStore(at pathInDocuments:String,with fileManager:FileManager) {
    try? fileManager.removeItem(at:fileURL(pathInDocuments:pathInDocuments))
}

private func fileURL(pathInDocuments:String,fileManager:FileManager = .default) -> URL {
    return URL.documentsDirectory.appending(component: pathInDocuments)
}
