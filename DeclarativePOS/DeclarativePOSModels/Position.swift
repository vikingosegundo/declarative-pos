//
//  Position.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 01.06.24.
//

public struct Position {
    public let id    : UUID
    public let amount: Int
    public let item  : StockItem
    
    public init(amount: Int, item: StockItem) {
        self.id     = UUID()
        self.amount = amount
        self.item   = item
    }
}

extension Position:Identifiable, Equatable, Codable {}
