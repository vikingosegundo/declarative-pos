//
//  Table.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 01.06.24.
//

public struct Table {
    public enum Change {
        case adding(Add); public enum Add {
            case order(Order)
        }
        case changing(Changing); public enum Changing {
            case name(to:String)
        }
    }
    public let id    : UUID
    public let name  : String
    public let orders: [Order]
    
    public init(name:String) {
        self.init(UUID(), name, [])
    }
    
    private init(_ id:UUID,_ name: String,_ orders: [Order]) {
        self.id     = id
        self.name   = name
        self.orders = orders
    }
    
    public  func alter(by c:Change...) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    private func alter(by c:Change   ) -> Self {
        switch c {
        case let .changing(.name(to:n)): Self(id, n   , orders      )
        case let .adding  (.order  (o)): Self(id, name, orders + [o])
        }
    }
}

extension Table: Identifiable, Equatable, Codable {}
