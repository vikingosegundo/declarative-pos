//
//  Order.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 01.06.24.
//

public struct Order {
    public enum Change {
        case adding(Add); public enum Add {
            case position(Position)
        }
        case removing(Removing); public enum Removing {
            case position(Position)
        }
        case changing(Changing); public enum Changing {
            case state(State)
        }
    }
    public enum State {
        case unsent
        case   sent
    }
    public let id       : UUID
    public let positions: [Position]
    public let created  : Date
    public let changed  : Date
    public let state    : State
    
    public init() {
        let date = Date.now
        self.init(UUID(),[], date, date, .unsent)
    }
    
    public  func alter(by c:Change...) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    private func alter(by c:Change   ) -> Self {
        switch c {
        case let .adding  (.position(p)): Self(id, positions + [p]                , created, .now, state)
        case let .removing(.position(p)): Self(id, positions.filter{$0.id != p.id}, created, .now, state)
        case let .changing(.state   (s)): Self(id, positions                      , created, .now, s    )
        }
    }
    
    private init(_ id:UUID,_ positions: [Position],_ created: Date,_ changed: Date,_ state: State) {
        self.id        = id
        self.positions = positions
        self.created   = created
        self.changed   = changed
        self.state     = state
    }
}

extension Order:Identifiable, Equatable, Codable {}
extension Order.State: Codable {}
