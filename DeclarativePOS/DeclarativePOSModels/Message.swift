//
//  Message.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 05.06.24.
//

public enum Message {
    case pos(POS); public enum POS {
        case cmd(CMD); public enum CMD {
            case add(Add); public enum Add {
                case table(Table)
                case stockItem(StockItem)
            }
            case remove(Remove); public enum Remove {
                case table(Table)
            }
            case update(Update); public enum Update {
                case table(Table)
            }
        }
        case ack(ACK); public enum ACK {
            case added(Added); public enum Added {
                case table(Table)
                case stockItem(StockItem)
            }
            case removed(Removed); public enum Removed {
                case table(Table)
            }
            case updated(Updated); public enum Updated {
                case table(Table)
            }
        }
    }
}
