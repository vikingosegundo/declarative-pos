//
//  StockItem.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 01.06.24.
//

public struct StockItem {
    public enum Change {
        case changing(Changing); public enum Changing {
            case name(to:String)
            case price(to:Double)
        }
    }
    public let id   : UUID
    public let name : String
    public let price: Double
    
    public init(name: String, price: Double) {
        self.init(UUID(),name, price)
    }
    private init(_ id:UUID,_ name:String,_ price:Double) {
        self.id    = id
        self.name  = name
        self.price = price
    }
    
    public  func alter(by c:Change...) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    private func alter(by c:Change   ) -> Self {
        switch c {
        case let .changing(.name (to:n)): Self(id,n   , price)
        case let .changing(.price(to:p)): Self(id,name, p    )
        }
    }
}

extension StockItem:Identifiable,Equatable,Codable {}
