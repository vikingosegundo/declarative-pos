//
//  AppState.swift
//  DeclarativePOSModels
//
//  Created by vikingosegundo on 01.06.24.
//

public struct AppState {
    public enum Change {
        case adding(Adding); public enum Adding {
            case table(Table)
            case stockItem(StockItem)
        }
        case removing(Removing); public enum Removing {
            case table(Table)
        }
        case updating(Updating); public enum Updating {
            case table(Table)
        }
    }
    
    public let tables: [Table]
    public let stockItems:[StockItem]
    
    public init () {
        self.init([],[])
    }
    private init(_ tables: [Table],_ stockItems:[StockItem]) {
        self.tables = tables
        self.stockItems = stockItems
    }
    public  func alter(by c:[Change] ) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    public  func alter(by c:Change...) -> Self { c.reduce(self) { return $0.alter(by:$1) } }
    private func alter(by c:Change   ) -> Self {
        switch c {
        case let .adding(    .table(t)): Self(tables + [t]                ,stockItems)
        case let .adding(.stockItem(i)): Self(tables, stockItems+[i])
        case let .removing(  .table(t)): Self(tables.filter{$0.id != t.id},stockItems)
        case let .updating(  .table(t)):
            tables.contains(where: { $0.id == t.id})
            ? self.alter(by: 
                    .removing(.table(t)),
                    .adding(.table(t)))
            : self
        }
    }
}

extension AppState:Codable {}
