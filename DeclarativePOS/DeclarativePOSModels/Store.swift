//
//  Store.swift
//  ELPModelsSpecs
//
//  Created by Manuel Meyer on 11.08.22.
//

public typealias Access<S> = (                    ) -> S  // get current state
public typealias Change<C> = ( C...               ) -> () // change state by aplying Change C{1.*}
public typealias Reset     = (                    ) -> () // reset to defaults
public typealias Updated   = ( @escaping () -> () ) -> () // subscriber
public typealias Destroy   = (                    ) -> () // destroy persistent data

public typealias Store<S,C> = ( /*S:State, C:Change*/
      state: Access<S>,
     change: Change<C>,
      reset: Reset,
    updated: Updated,
    destroy: Destroy
)

public func destroy<S,C>(_  store:inout Store<S,C>!){ store.destroy();store = nil }
