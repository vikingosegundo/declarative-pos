//
//  TableViewModel.swift
//  DeclarativePOSViewModels
//
//  Created by vikingosegundo on 02.06.24.
//

import SwiftUI
import DeclarativePOSModels

@Observable public class TableViewModel {
    public init(table: DeclarativePOSModels.Table, update: @escaping (TableViewModel) -> ()) {
        self.backedTable = table
        self.name = table.name
        self.orders = table.orders
        self.update = update
    }
    public var table:DeclarativePOSModels.Table { backedTable }
    public var id:UUID { backedTable.id }
    public var name  : String { didSet { backedTable = backedTable.alter(by: .changing(.name(to: name))) } }
    public private(set) var orders: [Order]
    
    private var backedTable:DeclarativePOSModels.Table { didSet { update(self) } }
    private var update:(TableViewModel) -> ()
    public func add(order:Order) {
        orders.append(order)
        backedTable = backedTable.alter(by: .adding(.order(order)))
    }
}

extension TableViewModel:Identifiable, Equatable {
    public static func == (lhs: TableViewModel, rhs: TableViewModel) -> Bool {
        lhs.id == rhs.id
    }
}
