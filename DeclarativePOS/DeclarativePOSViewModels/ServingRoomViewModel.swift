//
//  ServingRoomViewModel.swift
//  DeclarativePOSViewModels
//
//  Created by vikingosegundo on 02.06.24.
//

import SwiftUI
import DeclarativePOSModels

@Observable public class ServingRoomViewModel {
    public var tables: [TableViewModel] = []
    public var stockItems: [StockItem] = []
    public init(store:Store<AppState,AppState.Change>, roothandler r: @escaping(Message) -> ()) {
        roothandler = r
        store.updated { self.process(store.state()) }
        process(store.state())
    }
    
    public func add(_ t:DeclarativePOSModels.Table) { roothandler(.pos(.cmd(.add(.table(t))))) }
    public func add(_ i:StockItem) { roothandler(.pos(.cmd(.add(.stockItem(i))))) }
    public func update(_ t:TableViewModel) { roothandler(.pos(.cmd(.update(.table(t.table))))) }
    public func delete(_ t:TableViewModel) { roothandler(.pos(.cmd(.remove(.table(t.table))))) }
    
    private func process(_ appState:AppState) {
        DispatchQueue.main.async {
            self.tables = appState.tables.map({
                TableViewModel(table:$0, update: self.update)
            })
            self.stockItems = appState.stockItems
        }
    }
    
    private let roothandler:(Message) -> ()
}
