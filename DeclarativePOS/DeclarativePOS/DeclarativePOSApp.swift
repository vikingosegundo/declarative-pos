//
//  DeclarativePOSApp.swift
//  DeclarativePOS
//
//  Created by vikingosegundo on 01.06.24.
//

import SwiftUI
import DeclarativePOSUI
import DeclarativePOSModels
import DeclarativePOSViewModels
import DeclarativePOSApp

@main
struct DeclarativePOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(servingRoomViewModel:servingRoomVM)
        }
    }
}
fileprivate let store = createDiskStore()
fileprivate let servingRoomVM = ServingRoomViewModel(store: store, roothandler: { roothandler($0) })
fileprivate let roothandler = createAppDomain(store: store, receivers: [], rootHandler: { roothandler($0) })
