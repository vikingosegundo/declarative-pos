//
//  TableDetailView.swift
//  DeclarativePOSUI
//
//  Created by vikingosegundo on 06.06.24.
//

import SwiftUI
import DeclarativePOSViewModels
import DeclarativePOSModels

struct TableDetailView:View {
    @Binding var table:TableViewModel
    @State private var order: Order?

    var body: some View {
        VStack{
            List {
                ForEach(table.orders) {
                    Text("\($0.created)")
                }
            }
        }.toolbar{
            Button {
                order = Order()
            } label: {
                Image(systemName: "plus")
            }
        }.fullScreenCover(item: $order) { order in
            OrderView(table: $table, order: order)
        }
    }
}
