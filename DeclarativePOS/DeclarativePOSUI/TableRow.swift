//
//  TableRow.swift
//  DeclarativePOSUI
//
//  Created by vikingosegundo on 06.06.24.
//

import SwiftUI
import DeclarativePOSViewModels
import DeclarativePOSModels

public struct TableRow: View {
    @Binding var table: TableViewModel
    @Environment(ServingRoomViewModel.self) private var servingRoomViewModel
    public var body: some View {
        VStack {
            HStack {
                Text(table.name)
            }
           .contextMenu {
               Button(role:.destructive) { servingRoomViewModel.delete(table) } label: { Text("delete") }
           }
        }
    }
}
