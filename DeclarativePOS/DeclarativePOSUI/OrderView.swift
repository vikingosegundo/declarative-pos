//
//  OrderView.swift
//  DeclarativePOSUI
//
//  Created by vikingosegundo on 07.06.24.
//

import SwiftUI
import DeclarativePOSViewModels
import DeclarativePOSModels

struct OrderView: View {
    @Binding var table: TableViewModel
    @State var order:Order = Order()
    @Environment(ServingRoomViewModel.self) private var servingRoomViewModel
    @State private var presentTitleInput = false
    @State private var enteredTitle = ""
    @State private var enteredPrice = ""

    @Environment(\.dismiss) private var dismiss
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    Text("\(order.created)")
                    List {
                        ForEach(order.positions) {
                            Text($0.item.name)
                        }
                    }
                    HStack {
                        Button {
                            table.add(order:order)
                            dismiss()
                        } label: {
                            Text("send")
                        }
                        Button {
                            dismiss()
                        } label: {
                            Text("cancel")
                        }
                    }
                    List {
                        ForEach(servingRoomViewModel.stockItems) {
                            Text("\($0.name) \($0.price)")
                        }
                    }
                }.toolbar { Button { askTitleAndPriceForNewStockItem()  } label: { Image(systemName:"plus") } }
            }
        }.alert("New Stock Item",
                isPresented: $presentTitleInput,
                actions: {
            TextField("Stock Item name", text:$enteredTitle)
            TextField("Stock Item priceeCoff", text:$enteredPrice)
            Button               {   save() } label: { Text("Add stock item") }
            Button(role:.cancel) { cancel() } label: { Text("Cancel") } },
                message: { Text("Please enter title for new table.") }
         )
    }
}

private extension OrderView {
    func save() {
        saveToList()
    }
    func askTitleAndPriceForNewStockItem() {
        presentTitleInput = true
    }
    func cancel() {
        reset()
    }
    func reset() {
        enteredTitle = ""
        enteredPrice = ""
    }
}
private extension OrderView {
    func saveToList() {
        !enteredTitle.trimmingCharacters(in: .whitespaces).isEmpty
            ? servingRoomViewModel.add(StockItem(name: enteredTitle.trimmingCharacters(in: .whitespaces), price: Double(enteredPrice) ?? 0.0))
            : ()
        reset()
    }
}
