//
//  ContentView.swift
//  DeclarativePOS
//
//  Created by vikingosegundo on 01.06.24.
//

import SwiftUI
import DeclarativePOSModels
import DeclarativePOSViewModels

public struct ContentView: View {
    @State private var servingRoomViewModel: ServingRoomViewModel
    @State private var presentTitleInput = false
    @State private var enteredTitle = ""
    
    public init(servingRoomViewModel svm: ServingRoomViewModel) {
        servingRoomViewModel = svm
    }
    public var body: some View {
        VStack {
            NavigationView {
                List {
                    Section("Tables") {
                        if !tables.isEmpty {
                            ForEach(tables) { t in
                                NavigationLink(destination: TableDetailView(table:t)) {
                                    TableRow(table: t)
                                }
                            }
                        } else {
                            Text("No tables found")
                        }
                    }
                }.toolbar { Button { askTitleForNewTable() } label: { Image(systemName:"plus") } }
            }.navigationTitle("Tables")
        }
        .environment(servingRoomViewModel)
        .alert("New Table",
               isPresented: $presentTitleInput,
               actions: {
            TextField("Table name", text:$enteredTitle)
            Button               {   save() } label: { Text("Add table") }
            Button(role:.cancel) { cancel() } label: { Text("Cancel") } },
               message: { Text("Please enter title for new table.") }
        )
    }
    
    private func bind(_ table:TableViewModel) -> Binding<TableViewModel> {
        Binding(
            get: { table },
            set: { servingRoomViewModel.tables[servingRoomViewModel.tables.firstIndex(of:table)!] = $0 }
        )
    }
    private var tables: [Binding<TableViewModel>] {
        servingRoomViewModel.tables.map {
            bind($0)
        }
    }
}


private extension ContentView {
    func save() {
        saveToList()
    }
    func askTitleForNewTable() {
        presentTitleInput = true
    }
    func cancel() {
        reset()
    }
    func reset() {
        enteredTitle = ""
    }
}

private extension ContentView {
    func saveToList() {
        !enteredTitle.trimmingCharacters(in: .whitespaces).isEmpty
            ? servingRoomViewModel.add(Table(name:enteredTitle.trimmingCharacters(in:.whitespaces)))
            : ()
        reset()
    }
}

