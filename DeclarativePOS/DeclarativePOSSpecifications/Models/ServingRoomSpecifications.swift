//
//  ServingRoomSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 02.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class ServingRoomSpecifications:QuickSpec {
    override class func spec() {
        describe("ServingRoom") {
            var servingRoom:ServingRoom!
            beforeEach {
                servingRoom = ServingRoom(name: "Serving Room")
            }
            afterEach {
                servingRoom = nil
            }
            context("newly created") {
                it("has a name"   ) { expect(servingRoom.name  ).to(equal("Serving Room")) }
                it("has no tables") { expect(servingRoom.tables).to(beEmpty())             }
            }
            context("adding table") {
                var servingRoom1:ServingRoom!
                var table:Table!
                beforeEach {
                    table        = Table(name: "table 1")
                    servingRoom1 = servingRoom.alter(by: .adding(.table(table)))
                }
                afterEach {
                    table        = nil
                    servingRoom1 = nil
                }
                it("has one table"     ) { expect(servingRoom1.tables).to(equal([table]))          }
                it("has unchanged name") { expect(servingRoom1.name  ).to(equal(servingRoom.name)) }
            }
            context("renaming") {
                var servingRoom1:ServingRoom!
                beforeEach {
                    servingRoom1 = servingRoom.alter(by: .changing(.name("Terrace")))
                }
                afterEach {
                    servingRoom1 = nil
                }
                it("has new name"        ) { expect(servingRoom1.name  ).to(equal("Terrace"))          }
                it("has unchanged tables") { expect(servingRoom1.tables).to(equal(servingRoom.tables)) }
            }
        }
    }
}
