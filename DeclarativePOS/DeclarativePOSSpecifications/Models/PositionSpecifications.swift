//
//  PositionSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 01.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class PositionSpecifications:QuickSpec {
    override class func spec() {
        describe("Postion") {
            var position:Position!
            var item:StockItem!
            beforeEach {
                item     = StockItem(name: "Coffee", price: 3.0)
                position = Position(amount: 1, item: item)
            }
            afterEach {
                position = nil
                item     = nil
            }
            context("newly created") {
                it("has an id"      ) { expect(position.id    ).toNot(beNil())     }
                it("has an amount"  ) { expect(position.amount).to   (equal(1))    }
                it("has a stockitem") { expect(position.item  ).to   (equal(item)) }
            }
        }
    }
}
