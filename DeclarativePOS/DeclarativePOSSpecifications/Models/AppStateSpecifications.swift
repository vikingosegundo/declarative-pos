//
//  AppStateSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 01.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class AppStateSpecifications:QuickSpec {
    override class func spec() {
        describe("AppState") {
            var appState:AppState!
            beforeEach {
                appState = AppState()
            }
            afterEach {
                appState = nil
            }
            context("newly created") {
                it("has no tables") { expect(appState.tables).to(beEmpty()) }
            }
            context("adding table") {
                var appState1: AppState!
                var table    : Table!
                beforeEach {
                    table     = Table(name: "Table 1")
                    appState1 = appState.alter(by: .adding(.table(table)))
                }
                afterEach {
                    table     = nil
                    appState1 = nil
                }
                it("has one table") { expect(appState1.tables).to(equal([table])) }
            }
            context("removing table") {
                var appState1: AppState!
                var table    : Table!
                beforeEach {
                    table = Table(name: "Table 1")
                    appState1 = appState.alter(by: .adding(.table(table)))
                    appState1 = appState1.alter(by: .removing(.table(table)))
                }
                it("has no tables") { expect(appState1.tables).to(beEmpty()) }
            }
        }
    }
}
