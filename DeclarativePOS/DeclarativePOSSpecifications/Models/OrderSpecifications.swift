//
//  OrderSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 01.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class OrderSpecifications: QuickSpec {
    override class func spec() {
        describe("Order") {
            var order: Order!
            beforeEach {
                order = Order()
            }
            afterEach {
                order = nil
            }
            context("newly created") {
                it("has no positions"                        ) { expect(order.positions).to(beEmpty())            }
                it("has a creation date"                     ) { expect(order.created  ).to(beCloseTo(.now))      }
                it("has a change date equal to creation date") { expect(order.changed  ).to(equal(order.created)) }
                it("has a send state: unsent"                ) { expect(order.state    ).to(equal(.unsent))       }
             }
            context("adding positon") {
                var order1  : Order!
                var position: Position!
                var item    : StockItem!
                beforeEach {
                    item     = StockItem(name: "Coffee", price: 3.0)
                    position = Position(amount: 2, item: item)
                    order1   = order.alter(by: .adding(.position(position)))
                }
                it("has one position"           ) { expect(order1.positions).to   (equal([position]))    }
                it("has new change date"        ) { expect(order1.changed  ).toNot(equal(order.changed)) }
                it("has unchanged creation date") { expect(order1.created  ).to   (equal(order.created)) }
                it("has unchanged send state"   ) { expect(order1.state    ).to   (equal(order.state))   }
            }
            context("removing position") {
                var order1  : Order!
                var order2  : Order!
                var position: Position!
                var item    : StockItem!
                beforeEach {
                    item     = StockItem(name: "Coffee", price: 3.0)
                    position = Position(amount: 1, item: item)
                    order1   = order.alter(by: .adding(.position(position)))
                    order2   = order1.alter(by: .removing(.position(position)))
                }
                afterEach {
                    item     = nil
                    position = nil
                    order1   = nil
                    order2   = nil
                }
                it("has no positon"             ) { expect(order2.positions).to   (beEmpty())            }
                it("has a new change date"      ) { expect(order2.changed  ).toNot(equal(order.changed)) }
                it("has unchnaged creation date") { expect(order2.created  ).to   (equal(order.created)) }
                it("has unchanged send state"   ) { expect(order2.state    ).to   (equal(order.state))   }
            }
            context("changing state") {
                var order1 :Order!
                beforeEach {
                    order1 = order.alter(by: .changing(.state(.sent)))
                }
                afterEach {
                    order1 = nil
                }
                it("has new state: sent"        ) { expect(order1.state    ).to   (equal(.sent))           }
                it("has new change date"        ) { expect(order1.changed  ).toNot(equal(order.changed))   }
                it("has unchanged position"     ) { expect(order1.positions).to   (equal(order.positions)) }
                it("has unchanged creation date") { expect(order1.created  ).to   (equal(order.created))   }
            }
        }
    }
}
