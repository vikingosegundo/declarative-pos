//
//  TableSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 01.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class TableSpecifications:QuickSpec {
    override class func spec() {
        describe("Table") {
            var table:Table!
            beforeEach {
                table = Table(name: "Table 1")
            }
            afterEach {
                table = nil
            }
            context("newly created") {
                it("has a name"   ) { expect(table.name  ).to(equal("Table 1")) }
                it("has no orders") { expect(table.orders).to(beEmpty())        }
            }
            context("adding order") {
                var table1:Table!
                var order:Order!
                beforeEach {
                    order  = Order()
                    table1 = table.alter(by: .adding(.order(order)))
                }
                afterEach {
                    order  = nil
                    table1 = nil
                }
                it("has one order"     ) { expect(table1.orders).to(equal([order]))    }
                it("has unchanged name") { expect(table1.name  ).to(equal(table.name)) }
            }
            context("changing name") {
                var table1: Table!
                beforeEach {
                    table1 = table.alter(by: .changing(.name(to:"VIP Table")))
                }
                afterEach {
                    table1 = nil
                }
                it("has new name"         ) { expect(table1.name  ).to(equal("VIP Table"))  }
                it("hase unchanged orders") { expect(table1.orders).to(equal(table.orders)) }
            }
        }
    }
}
