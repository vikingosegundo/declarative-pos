//
//  StockItemSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 01.06.24.
//

import Quick
import Nimble
import DeclarativePOSModels

final class StockItemSpecifications: QuickSpec {
    override class func spec() {
        describe("StockItem") {
            var item:StockItem!
            beforeEach {
                item = StockItem(name: "Coffee", price: 3.0)
            }
            afterEach {
                item = nil
            }
            context("newly created") {
                it("has a name" ) { expect(item.name ).to(equal("Coffee")) }
                it("has a price") { expect(item.price).to(equal(3.0))      }
            }
            context("changing name") {
                var item1:StockItem!
                beforeEach {
                    item1 = item.alter(by: .changing(.name(to:"Americano")))
                }
                it("has new name"       ) { expect(item1.name ).to(equal("Americano")) }
                it("has unchanged price") { expect(item1.price).to(equal(item.price))  }
            }
            context("changing price") {
                var item1:StockItem!
                beforeEach {
                    item1 = item.alter(by: .changing(.price(to:3.5)))
                }
                afterEach {
                    item1 = nil
                }
                it("has new price"     ) { expect(item1.price).to(equal(3.5))       }
                it("has unchanged name") { expect(item1.name ).to(equal(item.name)) }
            }
        }
    }
}
