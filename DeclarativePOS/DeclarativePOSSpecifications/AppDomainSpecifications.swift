//
//  AppDomainSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSApp
import DeclarativePOSModels
import Quick
import Nimble

final class AppDomainSpecifications:QuickSpec {
    override class func spec() {
        describe("AppDomain") {
            var app:Input!
            var store: Store<AppState, AppState.Change>!
            var rootHandler: ((Message) -> ())!
            beforeEach {
                store = createDiskStore(pathInDocs: "AppDomainSpecifications.json")
                rootHandler = { msg in }
                app = createAppDomain(store:store,receivers:[],rootHandler:rootHandler)
            }
            afterEach {
                destroy(&store)
                rootHandler = nil
                app = nil
            }
            context("send add table command"){
                var table:Table!
                beforeEach {
                    table = Table(name:"Table 1")
                    app(.pos(.cmd(.add(.table(table)))))
                }
                afterEach {
                    table = nil
                }
                it("adds table to the store") { expect(store.state().tables).to(equal([table])) }
            }
            context("send remove table command"){
                var table0:Table!
                var table1:Table!
                beforeEach {
                    table0 = Table(name:"Table 1")
                    table1 = Table(name:"Table 2")
                    app(.pos(.cmd(.add(.table(table0)))))
                    app(.pos(.cmd(.add(.table(table1)))))
                    app(.pos(.cmd(.remove(.table(table0)))))
                }
                afterEach {
                    table0 = nil
                    table1 = nil
                }
                it("removes table from store") { expect(store.state().tables).to(equal([table1])) }
            }
            context("update table") {
                var table0:Table!
                beforeEach {
                    table0 = Table(name: "table 1")
                    app(.pos(.cmd(.add(.table(table0)))))
                    app(.pos(.cmd(.update(.table(store.state().tables.first!.alter(by: .changing(.name(to: "Table 1"))))))))
                }
                afterEach {
                    table0 = nil
                }
                it("has one table"                ) { expect(store.state().tables            ).to(haveCount(1))     }
                it("has one table named «Table 1»") { expect(store.state().tables.first!.name).to(equal("Table 1")) }
            }
            context("send add stockitem command") {
                var stockItem0:StockItem!
                beforeEach {
                    stockItem0 = StockItem(name: "Coffee", price: 3.5)
                    app(.pos(.cmd(.add(.stockItem(stockItem0)))))
                }
                afterEach {
                    stockItem0 = nil
                }
                it("has one stockitem"               ) { expect(store.state().stockItems             ).to(equal([stockItem0])) }
                it("has one stockitem named «Coffee»") { expect(store.state().stockItems.first!.name ).to(equal("Coffee"))     }
                it("has one stockitem priced «3.5»"  ) { expect(store.state().stockItems.first!.price).to(equal(3.5))          }
            }
        }
    }
}
