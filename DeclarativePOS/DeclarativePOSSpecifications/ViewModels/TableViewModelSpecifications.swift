//
//  TableViewModelSpecifications.swift
//  DeclarativePOSSpecifications
//
//  Created by vikingosegundo on 05.06.24.
//

import Quick
import Nimble
import DeclarativePOSViewModels
import DeclarativePOSModels

final class TableViewModelSpecifications:QuickSpec {
    override class func spec() {
        describe("TableViewModel") {
            var tableVM:TableViewModel!
            var table:Table!
            var updatedViewModel:TableViewModel!
            
            beforeEach {
                table = Table(name: "Table 1")
                tableVM = TableViewModel(table: table, update: { tvm in
                    updatedViewModel = tvm
                })
            }
            afterEach {
                tableVM = nil
                table = nil
                updatedViewModel = nil
            }
            context("newly created") {
                it("has a table"     ) { expect(tableVM.table).to(equal(table)) }
                it("has table's name") { expect(tableVM.name).to(equal("Table 1")) }
                it("has table's orders") { expect(tableVM.orders).to(equal(table.orders)) }
            }
            context("add order") {
                var order:Order!
                var item:StockItem!
                var position:Position!
                beforeEach {
                    item = StockItem(name: "Coffee", price: 3.0)
                    position = Position(amount: 2, item:item)
                    order = Order().alter(by: .adding(.position(position)))
                    tableVM.add(order:order)
                }
                afterEach {
                    order = nil
                    position = nil
                    order = nil
                }
                it("has it's updated callback triggered") { expect(updatedViewModel.orders).to(equal([order])) }
            }
        }
    }
}
