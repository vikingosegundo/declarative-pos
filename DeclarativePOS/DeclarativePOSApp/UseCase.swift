//
//  UseCase.swift
//  ELPMainApp
//
//  Created by Manuel Meyer on 19.08.22.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
    func request(to request:RequestType)
    // init(..., responder: @escaping (Response) -> ())
}
