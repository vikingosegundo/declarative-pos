//
//  createPOSFeature.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSModels

func createPOSFeature(
    store:Store<AppState, AppState.Change>,
    output: @escaping Output
) -> Input {
    
    let tableAdder     = TableAdder    (store:store,responder:process(out:output))
    let tableRemover   = TableRemover  (store:store,responder:process(out:output))
    let tableUpdater   = TableUpdater  (store:store,responder:process(out:output))
    let stockItemAdder = StockItemAdder(store:store,responder:process(out:output))
    func execute(cmd:Message.POS.CMD) {
        switch cmd {
        case let .add    (.table(t)): tableAdder    .request(to:.add   (t))
        case let .add(.stockItem(i)): stockItemAdder.request(to:.add   (i))
        case let .remove (.table(t)): tableRemover  .request(to:.remove(t))
        case let .update (.table(t)): tableUpdater  .request(to:.update(t))
        @unknown default: ()
        }
    }
    return {
        if case let .pos(.cmd(cmd)) = $0 { execute(cmd:cmd) }
    }
}

private func process(out: @escaping Output) -> (TableAdder.Response) -> () {
    {
        switch $0 {
        case let .added(table): out(.pos(.ack(.added(.table(table)))))
        }
    }
}

private func process(out: @escaping Output) -> (TableRemover.Response) -> () {
    {
        switch $0 {
        case let .removed(table): out(.pos(.ack(.removed(.table(table)))))
        }
    }
}

private func process(out: @escaping Output) -> (TableUpdater.Response) -> () {
    {
        switch $0 {
        case let .updated(table): out(.pos(.ack(.updated(.table(table)))))
        }
    }
}

private func process(out: @escaping Output) -> (StockItemAdder.Response) -> () {
    {
        switch $0 {
        case let .added(item): out(.pos(.ack(.added(.stockItem(item)))))
        }
    }
}
