//
//  TableAdder.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSModels

struct TableAdder:UseCase {
    enum Request {
        case add(Table)
    }
    enum Response {
        case added(Table)
    }
    
    init(store:Store<AppState,AppState.Change>, responder: @escaping (Response) -> ()) {
        interactor = Interactor(store:store,respond:responder)
    }
    
    func request(to request: Request) {
        switch request {
        case let .add(t): interactor.add(table:t)
        }
    }    
    
    private let interactor:Interactor
    typealias RequestType = Request
    typealias ResponseType = Response
}

private extension TableAdder {
    struct Interactor {
        let store:Store<AppState,AppState.Change>
        let respond: (Response) -> ()
        
        func add(table:Table) {
            store.change(.adding(.table(table)))
            respond(.added(table))
        }
    }
}
