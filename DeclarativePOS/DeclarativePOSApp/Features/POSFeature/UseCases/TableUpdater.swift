//
//  TableUpdater.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSModels

struct TableUpdater:UseCase {
    enum Request {
        case update(Table)
    }
    enum Response {
        case updated(Table)
    }
    public init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        interactor = Interactor(store:s,respond:r)
    }
    func request(to request: Request) {
        switch request {
        case let .update(t): interactor.update(table:t)
        }
    }
    private let interactor:Interactor
    typealias RequestType = Request
    typealias ResponseType = Response
}

private extension TableUpdater {
    struct Interactor {
        let store:Store<AppState,AppState.Change>
        let respond: (Response) -> ()
        func update(table:Table) {
            store.change(.updating(.table(table)))
            respond(.updated(table))
        }
    }
}

