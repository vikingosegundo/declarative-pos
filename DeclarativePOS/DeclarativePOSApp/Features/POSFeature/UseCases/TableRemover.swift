//
//  TableRemover.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSModels

struct TableRemover:UseCase {
    enum Request {
        case remove(Table)
    }
    enum Response {
        case removed(Table)
    }
    init(store:Store<AppState,AppState.Change>, responder: @escaping (Response) -> ()) {
        interactor = Interactor(store:store,respond:responder)
    }
    
    func request(to request: Request) {
        switch request {
        case let .remove(t): interactor.remove(table:t)
        }
    }
    
    private let interactor:Interactor
    typealias RequestType = Request
    typealias ResponseType = Response
}

extension TableRemover {
    struct Interactor {
        let store:Store<AppState,AppState.Change>
        let respond: (Response) -> ()
        func remove(table:Table) {
            store.change(.removing(.table(table)))
        }
    }
}
