//
//  StockItemAdder.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 07.06.24.
//

import DeclarativePOSModels

struct StockItemAdder:UseCase {
    enum Request {
        case add(StockItem)
    }
    enum Response {
        case added(StockItem)
    }
    public init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        interactor = Interactor(store:s,respond:r)
    }
    
    func request(to request: Request) {
        switch request {
        case let .add(i):interactor.add(stockItem:i)
        }
    }
    private let interactor:Interactor
    typealias RequestType = Request
    typealias ResponseType = Response
}

private extension StockItemAdder {
    struct Interactor {
        let store:Store<AppState,AppState.Change>
        let respond: (Response) -> ()
        func add(stockItem:StockItem) {
            store.change(.adding(.stockItem(stockItem)))
            respond(.added(stockItem))
        }
    }
}
