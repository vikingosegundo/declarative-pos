//
//  AppDomain.swift
//  DeclarativePOSApp
//
//  Created by vikingosegundo on 05.06.24.
//

import DeclarativePOSModels

public typealias  Input = (Message) -> ()
public typealias Output = (Message) -> ()

public func createAppDomain(
    store      : Store<AppState,AppState.Change>,
    receivers  : [Input],
    rootHandler: @escaping Output
) -> Input
{
    let features: [Input] = [
        createPOSFeature(store:store,output:rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach {
            $0(msg)
        }
    }
}
